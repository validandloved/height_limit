local the_height = minetest.settings:get("height_limit") or 310
local mesecons_how_close = minetest.settings:get("height_limit_mesecons_close") or 20
local deny_mesecons = minetest.settings:get("height_limit_mesecons") or true

minetest.register_on_placenode(function(pos, newnode, placer, oldnode, itemstack, pointed_thing)
    local name = placer:get_player_name()
    local is_mesecon = string.find(newnode.name, "mesecon")
    if is_mesecon == nil then
        is_mesecon = false
    else
        is_mesecon = true
    end
    if deny_mesecons == "false" then
        deny_mesecons = false
    end
        local howclose = mesecons_how_close-2
        if is_mesecon and deny_mesecons then
        if pos.y > the_height-howclose and pos.y < the_height+2 then
            minetest.remove_node(pos)
            local color = ""
            if math.random(1, 2) == 2 then
                color = "orange"
            else
                color = "yellow"
            end
            if math.random(1, 2) == 2 then
                minetest.chat_send_player(name, core.colorize(color, "Nope!"))
            else
                minetest.chat_send_player(name, core.colorize(color, "Nuh Uh."))
            end
            return itemstack
        end
        end
    if pos.y < the_height+2 then
         return
    end
    minetest.remove_node(pos)
    minetest.chat_send_player(name, core.colorize("red", string.format("You cant place block higher than %s!", the_height)))
    return itemstack
end)